# 必然性推理
## 直言命题
::: tip 解题方法
- 存在明显矛盾关系，则采用矛盾法解题；
- 若找不到明显的矛盾关系，则采用反对法解题。
:::
::: notitle
![avatar](../../imgs/timg.png)
:::

### 换质推理
::: notitle
双重否定表肯定
- 所有A是B→所有非A不是B
- 所有A不是B→所有A是非B
- 有些A是B→有些A不是非B
- 有些A不是B→有些A是非B
:::
### 换位推理
::: notitle
通过交换前提中直言命题的主项和谓项的位置，从而推出结论的推理方法。
- 所有A是B→有些B是A
- 所有A不是B→所有B不是A
- 有些A是B→有些B是A
- 有些A不是B不能进行换位推理
:::

## 复言命题
::: tip 解题方法
- 要从头读，别跳读，否则容易遗失关键点
- 遇到复杂的必然性推理，直接放弃
- 利用`矛盾关系`解题：由假言命题出发，看`与其肢命题`相似的`联言命题`是否是其矛盾命题
- 联言命题的矛盾命题是相容选言命题；相容选言命题的矛盾命题是联言命题。
:::

### 联言命题
::: notitle
- P且Q：同时为真才为真
- `矛盾命题：非P或非Q`
::: imtnotitle
- ……和……；不但……而且
- 虽然……但是……；不是……而是
:::

### 选言命题
::: notitle
#### 相容选言
- P或Q：至少一个为真才为真
- `矛盾命题：非P且非Q`
::: imtnotitle
- ……或……
- 或者……或者……
- 也许……也许
- 可能……也可能……
:::
::: notitle
#### 不相容选言
要么A，要么B：有且只有一个为真时为真。
::: imtnotitle
- 或者……或者……，二者不可兼得
- 要么……要么……
- 不是……就是……
:::

### 假言命题
::: notitle
#### 充分条件
- 如果A，那么B，则A是B的充分条件
- `前推后，否后推否前(逆否命题)，即A→B=非B→非A`
::: imtnotitle
- 如果……那么；
- 只要……就；
- 若……则；
- ……必须……
- 因为……所以
- 所有……都
:::
::: notitle
#### 必要条件
- A不成立，B一定不成立，A是B的必要条件
- `后推前，否前则否后(否定命题)，即A←B = 非A→非B`
::: imtnotitle
- 只有……才
- 没有……就没有……
- 除非……否则不……
- 不……不……
- ……是……的基础/假设/前提/关键
- ……是……的必要/先决/不可或缺条件
:::

<!-- ::: notitle
#### 充分条件和必要条件的转化
- 如果A那么B=只要B，才A
- 只有A，才B = 如果B，那么A
:::
::: notitle 
#### 除非A否则B
- A←B(只有A，才非B) = 非B→A(如果非B，那么A)= 如果非A，那么B
- 否一推一
::: -->

<!-- ## 概念和三段论
::: notitle 
#### 直言命题概念间关系
概念是构成命题和推理的基础，与词语不同，只有`表达了一类事物的词语才是概念`

- 四种概念间关系：
  - 全同关系
  - 真包含(于)关系
  - 交叉关系
  - 全异关系
- 解题方法
  - 依次分析出每两个概念之间的关系，然后结合文氏图进行解答。
  - 根据直言命题真假关系，画文氏图来进行解题。
:::

::: notitle
#### 三段论
三段论推理是由`两个直言命题作为前提`，`一个直言命题作为结论`而构成的推理。

#### 一特得特
- 前提中只要有一个是特称命题（有些），则结论也为特称命题
- 两个前提不能都是特称命题，即“有些”+“有些”推不出任何结论

#### 一否得否
- 前提中只要有一个是否定的，则结论也是否定的
- 两个前提不能都是否定

#### 三个概念出现两次
一个三段论中，`有且只能有三个不同的概念`，且`每个概念分别出现两次`
:::

::: notitle
#### 模态命题
- 模态命题：即含有必然，可能等模态词的命题。
- 负命题：一个命题前面加并非，等值于这个命题的矛盾命题。

#### 矛盾关系
模态命题前加上“并非”，即为其负命题，与原命题具有矛盾关系。
- 必然A和可能非A
- 必然非A和可能A
必有一真一假
::: -->

## 六个翻译四个等价
::: notitle
- 如果 P，那么 Q==只要 P，则 Q==P →Q
- 只有 P，才 Q==除非 P，否则不 Q == P 是 Q 必不可少的条件 ==Q →P
- 或者 P，或者 Q== 两者取其一 == －P→Q、－Q→P
:::
::: notitle
- 所有S都是P == 有的S是P / 有的P是S == S→P/ －P→－S 
- 所有S都不是P == 所有P都不是S == S→－P 
- 有的S是P == 有的P是S == 至少有个S是 P== 有的S→P
:::
::: notitle
- A→B 等价于 －B→－A
- A←B 等价于 -A→-B
- －(A或B) 等价于 －A且－B 
- －(A且B）等价于 －A或－B
:::

<bbt></bbt>
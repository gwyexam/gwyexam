# 类比推理
::: tip 解题方法
- 找关系，一种关系判断不出来，就发掘第二种关系
- 三个词可能有两种关系
::: notitle
- 代入排除法
- 遣词造句法
- 横纵对比法
  - 词性：近义反义（绝对反义词，相对反义词）
  - 感情色彩：褒义贬义
  - 差异：范围或程度；性质和功能
  - 词的构成：主谓，动宾
  - 必然与或然；全面与片面
:::

## 语义关系
::: notitle
#### 语法关系
- 主谓结构：主语+动词
- 动宾结构：动词+名词
- 并列结构
- 修饰结构
- 象征关系
:::

::: notitle
#### 近反义关系 
- 近义词
- 反义词 
::: imtnotitle
- 羔羊跪乳：乌鸦反哺
- 救援：围魏救赵
:::

## 描述关系
::: notitle
#### 事物
属性、功能、象征意义、活动空间、制作场所、所在地、原材料、作用对象等
- 属性分必然、或然；
- 功能分主要、次要；
- 原材料分必需与非必需

#### 人
职业与其特征、工作地点、对象、工具和历史事件、作品相关等
- 注意动作分施、受
:::

## 集合关系
::: notitle
- 全同关系：同一事物的不同称谓
- 包含关系
  - 种属关系：A是B的一种
  - 组成关系：A是B的一部分
  - 反种属关系：A不是B的一种
- 交叉关系：你中有我我中有你
- 并列关系：同一大类；共同属性
  - `矛盾`关系：两种状态
  - `反对`关系：多种状态中的两种
- 对应关系：目的
- 互补关系
:::
::: imtnotitle
#### 包含（种属）关系
- 闪电战：战术：突袭
- 建筑材料：混凝土：水泥
:::

::: imtnotitle
#### 并列关系
- 并列
- 并列且`加上某些条件`：如互相转化，时间先后等
  - 雨：雪
  - 电视机：游戏机
  - 书信：短信：微信
  - 扇子：电风扇：空调

#### 矛盾关系
- 曲 ：直
- 动 ：静
- 开灯：关灯

#### 反对关系
- 呼 ：吸
- 进 ：退
- 潮涨：潮落
- 起飞：降落
:::

::: imtnotitle
#### 交叉关系
- 微量元素：稀有金属：铜
- 内陆湖：淡水湖：青海湖
:::

## 逻辑关系
::: notitle
- `充要条件`关系
  - 只要……就：充分条件
  - 只有……才：必要条件
- `因果`关系：一个事件的发生引起另一个事件的发生
  - 必然因果
  - 或然因果

::: imtnotitle
#### 充要
- 水：森林：煤炭

#### 因果
- 竞争：淘汰（    ）
- 病毒：传染病：流行性
:::


<bbt></bbt>

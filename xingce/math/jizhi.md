# 极值问题

## 最值
![avatar](../../imgs/shorts/jizhi01.jpg)
![avatar](../../imgs/shorts/jizhi02.jpg)
![avatar](../../imgs/shorts/jizhi03.jpg)

## 抽屉原理
::: point 抽屉原理1
将多余n件的物品任意放到n个抽屉中，那么一定有一个抽屉中的物品件数不少于2。（至少有2件物品在同一个抽屉）
:::
::: point 抽屉原理2
将多余m * n件的物品任意放到n个抽屉中，那么一定有一个抽屉中的物品件数不少于m+1。（至少有m+1件物品在同一个抽屉）
:::
![avatar](../../imgs/shorts/jizhi03.jpg)


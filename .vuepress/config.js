module.exports = {
  title: "上岸",
  description: "Just playing around",
  plugins: [
    [
      "vuepress-plugin-container",
      {
        type: "point",
        before: (info) => `<div class="point"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "notitle",
        before: (info) => `<div class="notitle"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "sla",
        before: (info) => `<div class="sla"><div class="title">${info}</div>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "imtnotitle",
        before: (info) =>
          `<div class="imtnotitle"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "errorbook",
        before: (info) => `<div class="errorbook"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "imt",
        before: (info) => `<div class="imt"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "qst",
        before: (info) => `<div class="qst"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "imgcon",
        before: (info) =>
          `<div class="imgcontainer"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],
  ],
  base: "/gwyexam/",
  head: [
    // ...
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.css",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css",
      },
    ],
  ],
  port: 9325,
  markdown: {
    extendMarkdown: (md) => {
      md.use(require("markdown-it-katex"));
    },
  },
  themeConfig: {
    search: false,
    nav: [
      {
        text: "行测",
        items: [
          { text: "言语理解", link: "/xingce/yanyu/blanks" },
          { text: "判断推理", link: "/xingce/judgement/grapha" },
          { text: "资料分析", link: "/xingce/analyze/type" },
          { text: "数量关系", link: "/xingce/math/number" }
        ],
      },
      {
        text: "申论",
        items: [
          { text: "概述", link: "/shenlun/laoyang/yuedu" },
          { text: "小题", link: "/shenlun/laoyang/gaikuoti" },
          { text: "大作文", link: "/shenlun/article/template" }
        ]
      },
      {
        text: "事业单位",
        link: "/zongji/manager/guanli",
        items: [
          { text: "政治", link: "/zongji/polity/marxzhexue" },
          { text: "经济", link: "/zongji/economic/zzbasic" },
          { text: "管理", link: "/zongji/manager/guanli" },
          { text: "法律", link: "/zongji/law/lawgs" },
          { text: "常识", link: "/zongji/changshi/literature" },
          { text: "时政", link: "/zongji/shizheng/12" },
          { text: "公文", link: "/zongji/gongwen/jianjie" },
          // { text: "案例分析", link: "/zongying/anli/anli" },
          { text: "材料作文", link: "/zongying/zuowen/cailiao" },
          {
            text: "思维导图",
            items: [
              { text: "政治脑图", link: "/zongji/zongjisvg/polity" },
              { text: "经济脑图", link: "/zongji/zongjisvg/economic" },
              { text: "管理脑图", link: "/zongji/zongjisvg/manager" },
              { text: "法律脑图", link: "/zongji/zongjisvg/law" },
            ],
          },
        ],
      },
      { text: "面试", link: "/mianshi/mianshi" }
    ],
    sidebar: {
      "/xingce": [
        {
          title: "言语理解",
          collapsable: false,
          children: [
            {
              title: "逻辑填空",
              path: "/xingce/yanyu/blanks",
            },
            {
              title: "阅读理解",
              path: "/xingce/yanyu/purport",
            },
            {
              title: "语句表达",
              path: "/xingce/yanyu/sentence",
            },
          ]
        },
        {
          title: "判断推理",
          collapsable: false,
          path: "/xingce/judgement/cuoti",
          children: [
            {
              title: "图形推理",
              path: "/xingce/judgement/grapha",
            },
            {
              title: "定义判断",
              path: "/xingce/judgement/definition",
            },
            {
              title: "类比推理",
              path: "/xingce/judgement/analogy",
            },
            {
              title: "必然性推理",
              path: "/xingce/judgement/certain",
            },
            {
              title: "或然性推理-削弱加强",
              path: "/xingce/judgement/xueruojiaqiang",
            },
            {
              title: "或然性推理-其他",
              path: "/xingce/judgement/possible",
            }
          ]
        },
        {
          title: "资料分析",
          collapsable: false,
          path: "/xingce/analyze/base",
          children: [
            {
              title: "常见题型解析",
              path: "/xingce/analyze/type",
            },
            {
              title: "常用解题技巧",
              path: "/xingce/analyze/method",
            }
          ]
        },
        {
          title: "数量关系",
          collapsable: false,
          path: "/xingce/math/mengti",
          children: [
            {
              title: "数字推理",
              path: "/xingce/math/number",
            },
            {
              title: "数字特性",
              path: "/xingce/math/math",
            },
            {
              title: "工程问题",
              path: "/xingce/math/gongcheng",
            },
            {
              title: "行程问题",
              path: "/xingce/math/xingcheng",
            },
            {
              title: "利润问题",
              path: "/xingce/math/lirun",
            },
            {
              title: "比例问题",
              path: "/xingce/math/nongdu",
            },
            {
              title: "容斥问题",
              path: "/xingce/math/rongchi",
            },
            {
              title: "几何问题",
              path: "/xingce/math/jihe",
            },
            // {
            //   title: "极值问题",
            //   path: "/xingce/math/jizhi",
            // },
            {
              title: "排列组合与概率",
              path: "/xingce/math/pailie",
            },
            {
              title: "其他问题",
              path: "/xingce/math/other",
            }
          ]
        }
      ],
      "/shenlun": [
        // {
        //   title: "阿甘上岸说",
        //   path: "/shenlun/xiaoti/shenlunxiaoti",
        //   collapsable: false,
        //   children: [
        //     {
        //       title: "材料分类",
        //       path: "/shenlun/xiaoti/type",
        //     },
        //     {
        //       title: "材料要素",
        //       path: "/shenlun/xiaoti/element",
        //     },
        //     {
        //       title: "概括题",
        //       path: "/shenlun/xiaoti/generalize",
        //     },
        //     {
        //       title: "综合分析题",
        //       path: "/shenlun/xiaoti/synthesize",
        //     },
        //     {
        //       title: "对策题",
        //       path: "/shenlun/xiaoti/counterplan",
        //     },
        //     {
        //       title: "应用文写作",
        //       path: "/shenlun/xiaoti/practical",
        //     },
        //     {
        //       title: "议论文概述",
        //       path: "/shenlun/article/argumentation",
        //     },
        //     {
        //       title: "议论文写作方法",
        //       path: "/shenlun/article/method",
        //     }
        //   ],
        // },
        {
          title: "概述",
          collapsable: false,
          path: '/shenlun/laoyang/gaishu',
          children: [
            {
              title: '能力要求',
              path: '/shenlun/laoyang/gaikuo'
            },
            {
              title: '方法总览',
              path: '/shenlun/laoyang/yuedu'
            },
          ],
        },
        {
          title: "小题",
          collapsable: false,
          children: [
            {
              title: '概括题',
              path: '/shenlun/laoyang/gaikuoti'
            },
            {
              title: '综合分析题',
              path: '/shenlun/laoyang/zonghe'
            },
            {
              title: '对策题',
              path: '/shenlun/laoyang/duice'
            },
            {
              title: '汇总',
              path: '/shenlun/laoyang/huizong'
            }
          ]
        },
        {
          title: '大作文',
          collapsable: false,
          path: '/shenlun/laoyang/yingyongwen',
          children: [
            {
              title: "模板",
              path: "/shenlun/article/template"
            },
            
            {
              title: '范文',
              path: "/shenlun/article/fanwen"
            },
            // {
            //   title: "十步分析法",
            //   path: "/shenlun/article/muban",
            // },
            // {
            //   title: "现象类话题",
            //   path: "/shenlun/article/xianxiang",
            // },
            // {
            //   title: "概念类话题",
            //   path: "/shenlun/article/gainian",
            // },
          ]
        }
      ],
      "/zongji/polity": [
        {
          title: "马克思主义哲学",
          collapsable: false,
          children: [
            {
              title: "哲学概述",
              path: "/zongji/polity/marxzhexue",
            },
            {
              title: "辩证唯物论",
              path: "/zongji/polity/bianzhengweiwu",
            },
            {
              title: "唯物辩证法",
              path: "/zongji/polity/weiwubianzhengfa",
            },
            {
              title: "认识论",
              path: "/zongji/polity/renshilun",
            },
            {
              title: "社会历史观",
              path: "/zongji/polity/history",
            },
          ],
        },
        {
          title: "毛泽东思想",
          collapsable: false,
          children: [
            {
              title: "毛泽东思想概述",
              path: "/zongji/polity/mzd",
            },
            {
              title: "新民主主义革命理论",
              path: "/zongji/polity/mzddcll",
            },
            {
              title: "社会主义革命理论",
              path: "/zongji/polity/mzddcll2",
            },
            {
              title: "社会主义建设理论",
              path: "/zongji/polity/mzddcll3",
            },
          ],
        },
        {
          title: "中国特色社会主义理论体系",
          collapsable: false,
          children: [
            {
              title: "概述",
              path: "/zongji/polity/gaishu",
            },
            {
              title: "邓小平理论",
              path: "/zongji/polity/dxp",
            },
            {
              title: "三个代表",
              path: "/zongji/polity/jzm",
            },
            {
              title: "科学发展观",
              path: "/zongji/polity/hjt",
            },
            {
              title: "中国特色社会主义",
              path: "/zongji/polity/xjp",
            },
            {
              title: "党的十九大报告",
              path: "/zongji/polity/shijiuda",
            },
          ],
        },
        {
          title: "附录总结",
          collapsable: false,
          children: [
            {
              title: "重要会议",
              path: "/zongji/polity/meeting",
            },
            {
              title: "原则、特征",
              path: "/zongji/polity/yuanze",
            },
          ],
        },
      ],
      "/zongji/law": [
        {
          title: "法理学",
          collapsable: false,
          children: [
            {
              title: "法的概述",
              path: "/zongji/law/lawgs",
            },
            {
              title: "法律关系",
              path: "/zongji/law/lawgx",
            },
            {
              title: "法的运行",
              path: "/zongji/law/lawoperate",
            },
          ],
        },
        {
          title: "宪法",
          collapsable: false,
          children: [
            {
              title: "宪法基本理论",
              path: "/zongji/law/xfbase",
            },
            {
              title: "国家基本制度",
              path: "/zongji/law/zhengti",
            },
            {
              title: "公民的基本权利和义务",
              path: "/zongji/law/qlyw",
            },
            {
              title: "国家机构",
              path: "/zongji/law/jigou",
            },
          ],
        },
        {
          title: "民法",
          collapsable: false,
          children: [
            {
              title: "民法概论",
              path: "/zongji/law/mfzz",
            },
            {
              title: "民事法律关系",
              path: "/zongji/law/msflgx",
            },
            {
              title: "民事法律行为",
              path: "/zongji/law/msflxw",
            },
            {
              title: "合同法",
              path: "/zongji/law/hetong",
            },
            {
              title: "婚姻法",
              path: "/zongji/law/hunyin",
            },
            {
              title: "继承法",
              path: "/zongji/law/jicheng",
            },
          ],
        },
        {
          title: "刑法",
          collapsable: false,
          children: [
            {
              title: "刑法概述",
              path: "/zongji/law/xingfagaishu",
            },
            {
              title: "犯罪",
              path: "/zongji/law/fanzui",
            },
            {
              title: "刑罚",
              path: "/zongji/law/xingfa",
            },
          ],
        },
        {
          title: "行政法",
          collapsable: false,
          children: [
            {
              title: "行政法概述",
              path: "/zongji/law/xzfgaishu",
            },
            {
              title: "行政主体",
              path: "/zongji/law/xzzt",
            },
            {
              title: "行政行为",
              path: "/zongji/law/xzxw",
            },
            {
              title: "行政复议",
              path: "/zongji/law/fuyi",
            },
            {
              title: "公务员法",
              path: "/zongji/law/gwy",
            },
          ],
        },
        {
          title: "程序法",
          collapsable: false,
          children: [
            {
              title: "民事诉讼法",
              path: "/zongji/law/msss",
            },
            {
              title: "刑事诉讼法",
              path: "/zongji/law/xsss",
            },
            {
              title: "行政诉讼法",
              path: "/zongji/law/xzss",
            },
          ],
        },
        {
          title: "监察法",
          collapsable: false,
          children: [
            {
              title: "监察机关的设立及职责",
              path: "/zongji/law/jcjg",
            },
            {
              title: "监察范围、管辖和权限",
              path: "/zongji/law/jcfw",
            },
            {
              title: "监察程序",
              path: "/zongji/law/jccx",
            },
          ],
        },
        {
          title: "社会法",
          collapsable: false,
          children: [
            {
              title: "劳动法",
              path: "/zongji/law/laodong",
            },
            {
              title: "劳动合同法",
              path: "/zongji/law/laodonghetong",
            },
          ],
        },
      ],
      "/zongji/economic": [
        {
          title: "马克思主义政治经济学",
          collapsable: false,
          children: [
            {
              title: "政治经济学基本原理",
              path: "/zongji/economic/zzbasic",
            },
            {
              title: "剩余价值理论",
              path: "/zongji/economic/syjiazhi",
            },
          ],
        },
        {
          title: "经济体制",
          collapsable: false,
          children: [
            {
              title: "市场经济体制",
              path: "/zongji/economic/scjjtz",
            },
            {
              title: "社会主义市场经济体制",
              path: "/zongji/economic/shzyscjj",
            },
          ],
        },
        {
          title: "微观经济",
          collapsable: false,
          path: "/zongji/economic/weiguantotal",
          children: [
            {
              title: "市场体系",
              path: "/zongji/economic/sctx",
            },
            {
              title: "市场机制",
              path: "/zongji/economic/scjz",
            },
            {
              title: "市场结构",
              path: "/zongji/economic/scjg",
            },
          ],
        },
        {
          title: "宏观经济",
          collapsable: false,
          path: "/zongji/economic/hongguantotal",
          children: [
            {
              title: "宏观经济调控",
              path: "/zongji/economic/hgjjtk",
            },
            {
              title: "经济学术语与名词",
              path: "/zongji/economic/ecovac",
            },
          ],
        },
        {
          title: "国际经济",
          collapsable: false,
          path: "/zongji/economic/guojitotal",
          children: [
            {
              title: "开放经济",
              path: "/zongji/economic/kaifang",
            },
            {
              title: "国际金融 ☆",
              path: "/zongji/economic/gjjr",
            },
            {
              title: "世界经济组织",
              path: "/zongji/economic/zuzhi",
            },
          ],
        },
      ],
      "/zongji/manager": [
        {
          title: "管理学基本理论",
          collapsable: false,
          children: [
            {
              title: "管理概述",
              path: "/zongji/manager/guanli",
            },
            {
              title: "管理职能",
              path: "/zongji/manager/zhineng",
            },
            {
              title: "决策",
              path: "/zongji/manager/juece",
            },
            {
              title: "补充",
              children: [
                {
                  title: "管理学理论",
                  path: "/zongji/manager/gllilun",
                },
                {
                  title: "管理学原理",
                  path: "/zongji/manager/glyuanli",
                },
                {
                  title: "管理学效应、定律",
                  path: "/zongji/manager/glxiaoying",
                },
              ],
            },
          ],
        },
        {
          title: "行政管理基础知识",
          collapsable: false,
          path: "/zongji/manager/xingzhengtotal",
          children: [
            {
              title: "行政管理",
              path: "/zongji/manager/zhengfu",
            },
            {
              title: "行政组织",
              path: "/zongji/manager/zuzhi",
            },
            {
              title: "行政领导",
              path: "/zongji/manager/lingdao",
            },
            {
              title: "行政决策",
              path: "/zongji/manager/xzjuece",
            },
            {
              title: "行政执行",
              path: "/zongji/manager/zhixing",
            },
            {
              title: "行政监督",
              path: "/zongji/manager/jiandu",
            },
          ],
        },
        {
          title: "公共管理基础知识",
          collapsable: false,
          path: "/zongji/manager/gonggongtotal",
          children: [
            {
              title: "公共管理概述",
              path: "/zongji/manager/gonggong",
            },
            {
              title: "公共政策",
              path: "/zongji/manager/zhengce",
            },
            {
              title: "公共决策",
              path: "/zongji/manager/gonggongjuece",
            },
            {
              title: "公共危机管理",
              path: "/zongji/manager/weiji",
            },
          ],
        },
      ],
      "/zongji/gongwen": [
        {
          title: "公文基础知识",
          collapsable: false,
          children: [
            {
              title: "公文概述",
              path: "/zongji/gongwen/jianjie",
            },
            {
              title: "公文行文规则",
              path: "/zongji/gongwen/guize",
            },
            {
              title: "公文格式",
              path: "/zongji/gongwen/geshi",
            },
            {
              title: "公文撰写",
              path: "/zongji/gongwen/zhuanxie",
            },
            {
              title: "15种常用法定公文",
              path: "/zongji/gongwen/changyong",
            },
          ],
        },
        {
          title: "公文处理",
          collapsable: false,
          children: [
            {
              title: "公文拟制",
              path: "/zongji/gongwen/nizhi",
            },
            {
              title: "公文办理",
              path: "/zongji/gongwen/banli",
            },
            {
              title: "公文管理",
              path: "/zongji/gongwen/guanli",
            },
          ],
        },
      ],
      "/zongji/changshi": [
        {
          title: "文学常识",
          collapsable: false,
          children: [
            {
              title: "中国名家名篇",
              path: "/zongji/changshi/literature",
            },
            {
              title: "外国名家名篇",
              path: "/zongji/changshi/waiguo",
            },
          ],
        },
        {
          title: "科技艺术",
          collapsable: false,
          children: [
            {
              title: "科技成就",
              path: "/zongji/changshi/science",
            },
            {
              title: "书法绘画音乐",
              path: "/zongji/changshi/art",
            },
            {
              title: "器物雕塑",
              path: "/zongji/changshi/qiwu",
            },
            {
              title: "传统戏曲",
              path: "/zongji/changshi/xiqu",
            },
          ],
        },
        {
          title: "文化常识",
          collapsable: false,
          children: [
            {
              title: "历史典故",
              path: "/zongji/changshi/diangu",
            },
            {
              title: "文化思想",
              path: "/zongji/changshi/ideology",
            },
            {
              title: "人才选拔",
              path: "/zongji/changshi/rencai",
            },
            {
              title: "古代称谓",
              path: "/zongji/changshi/chengwei",
            },
            {
              title: "传统节日",
              path: "/zongji/changshi/jieri",
            },
            {
              title: "其他",
              path: "/zongji/changshi/other",
            },
          ],
        },
        {
          title: "历史常识",
          collapsable: false,
          children: [
            {
              title: "中国古代史",
              path: "/zongji/changshi/ancient",
            },
            {
              title: "中国近代史",
              path: "/zongji/changshi/earlymorden",
            },
            {
              title: "中国现代史",
              path: "/zongji/changshi/morden",
            },
            {
              title: "世界历史",
              path: "/zongji/changshi/worldhistory",
            },
          ],
        },

        {
          title: "生活常识",
          collapsable: false,
          children: [
            {
              title: "物理常识",
              path: "/zongji/changshi/wuli",
            },
            {
              title: "化学常识",
              path: "/zongji/changshi/huaxue",
            },
            {
              title: "生物常识",
              path: "/zongji/changshi/shengwu",
            },
            {
              title: "医学常识",
              path: "/zongji/changshi/yixue",
            },
            {
              title: "生活常识",
              path: "/zongji/changshi/shenghuo",
            },
            {
              title: "环保常识",
              path: "/zongji/changshi/huanbao",
            },
          ],
        },
      ],
      "/zongji/shizheng": [
        {
          title: "12月",
          path: "/zongji/shizheng/12",
        },
        {
          title: "11月",
          path: "/zongji/shizheng/11",
        },
        {
          title: "10月",
          path: "/zongji/shizheng/10",
        },
        {
          title: "9月",
          path: "/zongji/shizheng/09",
        },
        {
          title: "8月",
          path: "/zongji/shizheng/08",
        },
        {
          title: "7月",
          path: "/zongji/shizheng/07",
        },
        {
          title: "6月",
          path: "/zongji/shizheng/06",
        },
        {
          title: "5月",
          path: "/zongji/shizheng/05",
        },
        {
          title: "4月",
          path: "/zongji/shizheng/04",
        },
        {
          title: "3月",
          path: "/zongji/shizheng/03",
        },
        {
          title: "2月",
          path: "/zongji/shizheng/02",
        },
        {
          title: "1月",
          path: "/zongji/shizheng/01",
        },
      ],
      "/zongji/shiyedanwei": [
        {
          title: "事业单位概况",
          collapsable: false,
          children: [
            {
              title: "事业单位概述",
              path: "/zongji/shiyedanwei/sygaishu",
            },
            {
              title: "事业单位人事制度",
              path: "/zongji/shiyedanwei/syrenshi",
            },
            {
              title: "事业单位改革",
              path: "/zongji/shiyedanwei/sygaige",
            },
          ],
        },
        {
          title: "公民道德建设",
          collapsable: false,
          children: [
            {
              title: "公民道德概述",
              path: "/zongji/shiyedanwei/ddgaishu",
            },
            {
              title: "社会公德和家庭美德",
              path: "/zongji/shiyedanwei/gongde",
            },
            {
              title: "职业道德",
              path: "/zongji/shiyedanwei/zhiyedaode",
            },
          ],
        },
      ],
      "/zongying/anli": [
        {
          title: "案例分析",
          path: "/zongying/anli/anli",
        },
      ],
      "/zongying/zuowen": [
        {
          title: "材料作文考情",
          path: "/zongying/zuowen/cailiao",
        },
        {
          title: "材料作文写作",
          path: "/zongying/zuowen/article",
        },
      ],
      "/mianshi": [
        {
          title: "面试概述",
          path: "/mianshi/mianshi",
        },
        {
          title: "面试理论",
          collapsable: false,
          children: [
            {
              title: "1.综合分析",
              path: "/mianshi/zonghe",
            },
            {
              title: "2.组织管理",
              path: "/mianshi/zuzhi",
            },
            {
              title: "3.人际关系",
              path: "/mianshi/renji",
            },
            {
              title: "4.情景应变",
              path: "/mianshi/yingbian",
            },
            // {
            //   title: "5.人岗认知",
            //   path: "/mianshi/rengang",
            // },
            // {
            //   title: "6.演讲",
            //   path: "/mianshi/yanjiang",
            // },
            {
              title: '答题框架',
              path: "/mianshi/kuangjia"
            }
          ],
        },
        {
          title: "理论拓展",
          collapsable: false,
          children: [
            {
              title: '组织管理拓展',
              path: "/mianshi/zuzhituozhan",
            },
            {
              title: '人际关系拓展',
              path: "/mianshi/renshituozhan",
            }
          ]
        }
      ],
    },
    sidebarDepth: 2,
    prev: false,
    next: false
  },
};

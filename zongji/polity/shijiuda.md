# 党的十九大报告

## 中国特社会主义进入了新时代
我国社会主要矛盾发生变化(已转化为人民日益增长的美好生活需要和不平衡不充分发展之间的矛盾)。要把握两个没有变，三个牢牢。

::: tip 两个没有变
- 我国仍处于并将长期处于社会主义初级阶段的`基本国情`没有变
- 我国是世界上最大发展中国家的`国际地位`没有变
:::
::: tip 三个牢牢
- 牢牢`把握`社会主义初级阶段
- 牢牢`立足`社会主义初级阶段这个最大实际
- 牢牢`坚持`党的基本路线这个党和国家的`生命线`，人民的`幸福线`，领导和团结全国各族人民，以经济建设为中心，坚持四项基本原则，坚持改革开放，自力更生，艰苦创业，为把我国建成富强民主文明和谐美丽的社会主义现代化强国而奋斗。
:::

## 新时代中国共产党的使命
::: point 四个伟大
- 伟大斗争
- 伟大工程
- 伟大事业
- 伟大梦想

四个伟大紧密联系，相互贯通，相互作用，其中起决定性作用的是党的建设新的`伟大工程`。
:::
::: tip
- 中国特色社会主义道路 → `必由之路`
- 中国特色社会主义理论体系 → `正确理论`
- 中国特色社会主义制度 → `根本制度保障`
- 中国特色社会主义文化 → `强大的精神力量`
:::

## 全面小康，全面社会主义现代化
::: tip 两个重要时期
- 从现在到2020，全面建成小康社会`(十三五规划)`
- 从十九大到二十大，`两个一百年奋斗目标`的历史交汇期。
  - 到建党一百年时，全面建成小康社会；
  - 到新中国成立一百年时，全面建成社会主义现代化强国。
:::

::: tip 两个阶段
- 2020~2035基本实现社会主义现代化
- 2035~2050把我国建成富强民主文明和谐美丽的社会主义现代化强国
:::
::: notitle
从全面建成小康社会到基本实现社会主义现代化，再到全面建成社会主义现代化强国，是新时代中国特色社会主义发展的战略安排。
:::


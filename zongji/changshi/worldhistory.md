# 世界历史
## 世界大战
::: point 一战
- 萨拉热窝事件（1914-6-28）
- 凡尔登战役（1916-2-21~1916-12-19），破坏性最大时间最长
:::
::: point 二战
- 波兰战役或闪击波兰（1939-9-1）
- 斯大林格勒保卫战（1942-7~1943-2）
- 诺曼底登陆（1944-6-6）
:::
::: point 人物
- 丘吉尔，两次担任英国首相：
  - 1940~1945
  - 1951~1955
:::
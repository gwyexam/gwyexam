# 历史典故
## 人物
|  人物   | 典故  |
|  ----  | ----  |
| 齐桓公 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  | 尊王攘夷；老马识途；风马牛不相及；庭燎招士 |
| 管仲  | 管鲍之交；仓廪实而知礼节，衣食足而知荣辱 |
| 晋文公  | 秦晋之好；退避三舍 |
| 楚庄王  | 一鸣惊人；问鼎中原 |
| 越王勾践  | 卧薪尝胆；飞鸟尽，良弓藏；狡兔死，走狗烹 |
| 商鞅  | 徙木立信；作法自毙 |
| 廉颇、蔺相如  | 完璧归赵；负荆请罪；将相和 |
| 孙膑  | 围魏救赵；田忌赛马 |
| 赵括  | 纸上谈兵 |
| 吕不韦  | 奇货可居；一字千金 |
| 陈胜  | 燕雀焉知鸿鹄之志；苟富贵，勿相忘；王侯将相宁有种乎 |
| 刘邦  | 约法三章；明修栈道，暗渡陈仓 |
| 项羽  | 破釜沉舟（巨鹿之战） |
| 霍去病  | 冠军（侯）；封狼居胥 |

## 战争
|  战争   | 典故  |
|  ----  | ----  |
| 阪泉之战（炎帝与黄帝）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  |  |
| 涿鹿之战（炎帝联合黄帝与蚩尤）  |  |
| 鸣条之战（商朝与夏朝）  |  |
| 牧野之战（西周与商朝）  | 临阵倒戈 |
| 城濮之战（晋楚）  | 退避三舍 |
| 桂陵之战（魏赵）  | 围魏救赵 |
| 马陵之战（齐魏）  | 因势利导 |
| 长平之战（秦赵）  | 纸上谈兵 |
| 巨鹿之战（项羽和秦军）  | 破釜沉舟 |
| 楚汉战争（刘邦和项羽）  | 明修栈道，暗渡陈仓、背水一战、约法三章、十面埋伏 |
| 官渡之战（曹操与袁绍）  | 倒屣相迎 |
| 赤壁之战（孙权刘备与曹操) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  | 调兵遣将、自不量力、不计其数、丢盔弃甲、得意忘形、以卵击石、知彼知己、敌众我寡、敌强我弱、以少胜多、以弱胜强 |
| 淝水之战（东晋与前秦）  | 投鞭断流、风声鹤唳、草木皆兵 |
# 器物雕塑

## 古代器物
::: point 青铜器
代表中原文化的青铜国宝主要包括：
- 殷墟出土的商代后母戊大方鼎
- 陕西出土的西周大孟鼎、大克鼎、毛公鼎、何尊（何尊铭文中首次出现“中国”二字）
- 河南新郑郑公大墓出土的莲鹤方壶
:::
::: point 瓷器
- 唐代瓷器以“南青北白唐三彩”为代表。
  - “南青”指越窑的青瓷；
  - “北白”指邢窑的白瓷；
  - “唐三彩”指盛行于唐代的、以黄绿白三种颜色为主的一种低温釉陶器，因多出土于洛阳，有“洛阳唐三彩”之称。
- 宋代“五大名窑”分别是汝窑、官窑、哥窑、钧窑、定窑。
- 景德镇在明代成为全国制瓷中心。
- 景德镇有四大传统名瓷：`青花瓷、粉彩瓷、颜色釉瓷和玲珑瓷`。
:::

## 雕塑
::: notitle
#### 秦始皇陵兵马俑
迄今为止出土的世界上最大的艺术宝库，被誉为世界上“第八大奇迹”。

#### 两汉瑰宝
- 汉阳陵（汉景帝墓）出土上万件举世无双的陶俑，以仕女俑最为著名。
- 河北满城的西汉中山靖王陵，出土了长信宫灯、博山炉和金缕玉衣三件国宝级文物。
- 甘肃武威出土的东汉铜奔马，又名马踏飞燕，是国之重宝。

#### 石窟艺术
- 甘肃敦煌莫高窟、山西大同云冈石窟、河南洛阳龙门石窟、甘肃天水麦积山石窟被称为中国“四大石窟”。
- 它们是我国石窟艺术的精华。其中龙门石窟在唐代武则天时期十分兴盛。
:::
# 行政管理
## 行政管理的含义
::: qst 问题
行政管理的含义是什么？
:::
::: notitle
- 广义上，`公共组织`为实现`公共利益`，为社会提供`公共产品和服务`的活动。 
- 狭义上，`政府主导`的`管理`国家事务、经济文化事业、社会事务及机关内部事务，实现公共利益的活动。 
:::
::: notitle
- 管理主体——`行政组织` 
- 管理方式——`依法治国` 
- 管理对象——`国家和社会公共事务`
- 管理目的——`实现公共利益`
:::

## 行政管理的主体
::: qst 问题
行政管理的主体是谁？
:::
::: notitle
- 行政管理的主体是指实施公共行政管理行为的当事人，即`享有行政管理权力`，能`以自己的名义`实施有关管理行为，并`承担相应法律责任`的组织。 
- 行政管理的主体是特定的，在我国就是`各级人民政府及其所属的各类行政机构`。
  - `职权性行政组织`是根据《宪法》和《组织法》的规定，自产生起就具有公共行政主体资格的行政主体。
  - `授权性行政组织`是根据法律法规的授权而具有公共行政主体资格的行政主体。 
- 立法、司法机关不属于行政管理的主体，人民团体、群众组织、民间社团、企业等更不属于行政管理的主体。
:::

## 政府职能
::: qst 问题
- 行政主体的职能分为什么？分别包含什么内容？
:::
### 基本职能
::: point 政治职能
- 军事保卫职能
- 外交职能
- `治安`职能
- `民主建设`职能
:::
::: point 经济职能
- 宏观调控职能
- 提供公共产品和服务的职能
- `市场监管`职能
:::
::: point 文化职能
- 发展科学技术的职能
- 发展教育的职能
- 发展文化事业的职能
- 发展卫生体育的职能
:::
::: point 社会职能
- `调节社会分配和组织社会保障`的职能
- `保护生态环境和自然资源`的职能
- 促进社会化服务体系建立的职能
- 提高人口质量，实行计划生育的职能
:::

### 运行职能
::: notitle
- `计划职能`：行政管理运作程序中的`首要职能`，是管理的核心
- `组织职能`：体现政府行政`管理整体性和凝聚性`功能的职能，是实现行政管理目标和管理效能的关键性职能
- `协调职能`：行政管理过程中`平衡`各类行政关系、调节各种利益因素的职能
- `控制职能`：`防止和纠正`偏离目标行为的职能
:::

## 政府职能的转变
::: qst 问题
政府职能转变包括哪些方面？
:::
::: notitle
政府职能转变，是指国家行政机关在一定时期内对社会公共事务管理的职责、作用、功能的转换与发展变化。
1. 管理`职权、职责`的改变
2. 管理`角色`的转换
3. 管理`手段、方法及其模式`的转变等

政府职能的转变是从原来计划经济条件下政府无所不包的全能政府的管理职能转为`市场经济条件下有限的政府职能`
:::

::: qst 问题
- 政府职能重心和政府职能关系如何进行调整？
- 职能方式如何转变？
:::
::: notitle
- 政府职能重心的调整：由`“管制”向“服务”转变`。
- 政府职能关系的调整：`理顺内/外关系`。 
  - 外部关系：政企分开、政资分开、政事分开、政府与市场中介组织分开。
  - 内部关系：主要理顺中央与地方、上级与下级政府间及各 职能部门之间的关系。
- 职能方式的转变 
  - 从`直接、微观`管理向`间接、宏观`管理转变。 
  - 从依靠`单一的行政手段`向`综合运用经济、法律、行政`的转变。
- `政府职能转变是政府改革的关键`。
:::

::: qst 问题
- 如何使市场在资源配置中起决定性作用和更好发挥政府作用？
- 什么是“放管服”？
:::
::: notitle
`放管服，就是简政放权、放管结合、优化服务的简称`。 
 
- “放”即`简政放权`，降低准入门槛。 
- “管”即`公正监管`，促进公平竞争。 
- “服”即`高效服务`，营造便利环境。
:::
<bbt></bbt>
# 劳动合同法
## 劳动合同的分类
::: point 1.固定期限劳动合同
指用人单位与劳动者约定合同终止时间的劳动合同。
:::
::: point 2.无固定期限劳动合同
指用人单位与劳动者约定无确定终止时间的劳动合同。
::: point 应签订无固定期限劳动合同
- 劳动者在该用人单位`连续工作满10年`
- 用人单位单位`初次实行劳动合同制度`或者国有企业改制重新订立劳动合同时，劳动者在该用人单位`连续工作满十年且距法定退休年龄不足十年`
- `连续订立二次固定期限劳动合同`，且劳动者没有《中华人民共和国劳动合同法》第三十九条和第四十条第一项、第二项规定的情形，续订劳动合同的。
- 用人单位自`用工之日起满一年不与劳动者订立书面劳动合同的`，视为用人单位与劳动者已江立无固定期限劳动合同。
:::

::: point 3.工作任务劳动合同
可订立以完成一定工作任务为期限的劳动合同
:::

## 试用期规定
::: notitle
- 劳动合同期限3个月以上不满1年的，试用期不得超过`1个月`
- 劳动合同期限1年以上不满3年的，试用期不得超过`3个月`
- 3年以上固定期限和无固定期限的劳动合同，试用期不得超过`6个月`
:::
::: notitle
- 同一用人单位与同一劳动 只能约定一次试用期
- 以完成一定工作任务为期限的劳动合同或者劳动合同期限不满三个月的，`不得约定试用期`
- 试用期包含在劳动合同期限内。
- 劳动合同`仅约定试用期的，试用期不成立`，该期限为劳动合同期限
- 试用期工资不能低于百分之八十
:::

## 用人单位不得解除劳动合同的情形
::: notitle
- 从事接触职业病危害作业的劳动者未进行离岗前职业健康检查，或者疑似职业病病人在诊由或者医学观察期间的。
- 在本单位患职业病或因工负伤，并被确认丧失或者部分丧失劳动能力的
- 患病或非因工负伤，`在医疗期的`
- 女职工孕期、产期、哺乳期的
- 连续工作满十五年，且`距法定退休不满5年的`
- 法律、行政法规规定的其他情形
:::
::: point 劳动者可解除劳动合同
- 未按照劳动合同约定提供劳动保护或者劳动条件的；
- 未及时足额支付劳动报酬的；
- 未依法为劳动者缴纳社会保险费的
- 用人单位的规章制度违反法律、法规的规定，损害劳动者权益的；
:::


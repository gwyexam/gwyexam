# 劳动法
## 工作时间
::: notitle
- 用人单位不得违反《中华人民共和国劳动法》规定延长劳动者的工作时间
- 国家实行劳动者每日工作时间`不超过8小时`，平均每周工作时间`不超过44小时`的工时制度
- 用人单位由于生产经营需要，经与工会和劳动者协商后可以延长工作时间，`一般不超过1小时`；
- 特殊原因需要延长工作时间的，在保障劳动者身体健康的条件下延长工作时间`每日不得超过3小时但是每月不得超过36小时`
:::
::: imt 加班时间不受限制
- 发生自然灾害、事故或者因其他原因，威胁劳动者生命健康和财产安全，需要`紧急处理`的；
- 生产设备、交通运输线路、公共设施发生故障，影响生产和公众利益，必须`及时抢修`的；
- 法律、行政法规规定的其他情形。
:::

## 休息休假
::: notitle
- 用人单位应当保证劳动者`每周至少体息1日`
- 用人单位在下列节日期间应当依法安排劳动者休假：`元旦，春节，国际劳动节，国庆节`，法律、法规规定的其他休假节日。
:::

::: point 国家实行带薪年休假制度。
- 职工累计工作已满1年不满10年的，`年假5天`
- 已满10年不满20年的，`年假10天`
- 已满20年的，`年假15天`
:::
## 劳动争议的解决
当事人可以依法`申请调解`、`仲裁`、`提起诉讼`，也可以`协商解决`。

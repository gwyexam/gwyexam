# 监察范围、管辖和权限

## 监察范围
::: qst 问题
监察委员会的监察范围是？
:::
::: point 被监察人员
- 中国共产党机关、人大会及常委会、人民政府、监察委员会、人民法院、人民检察院、政协各级委员会机关、民主党派机关和工商业联合会机关的`公务员`，以及参照《中华人民共和国公务员法》管理的人员。
- 法律、法规授权或者受国家机关依法委托管理公共事务的组织中`从事公务的人员`。
- `国有企业管理人员`
- 公办的教育、科研、文化、医疗卫生、体育等`事业单位管理人员`。
- `基层群众性自治组织中从事管理的人员`。
- 其他依法`履行公职的人员`。
:::

## 管辖
::: qst 问题
监察机关之间存在争议时如何处理？
:::
::: notitle
- 监察机关之间对监察事项的管辖有争议的，由其`共同的上级监察机关`确定。 
- 上级监察机关可以将其所管辖的监察事项指定下级监察机关管辖，也可以将下级监察机关有管辖权的监察事项指定给其他监察机关管辖。 
- 监察机关认为所管辖的监察事项重大、复杂，需要由上级监察机关管辖的，可以报请上级监察机关管辖。
:::
## 监察权限
::: notitle
谈话、询问、讯问、留置、查询、冻结、搜查、调取、查封、扣押、勘验检查、鉴定等权限。
:::
::: qst 问题
什么情况下监察机关可以采取留置措施？
:::
::: notitle
- 涉及案情重大、复杂的；
- 可能逃跑、自杀的；
- 可能串供或者伪造、隐匿、毁灭证据的；
- 可能有其他妨碍调查行为的。
:::
::: qst 问题
什么情况下监察机关可以采取冻结措施？
:::
::: notitle
监察机关调查涉嫌`贪污贿赂`、`失职渎职`等严重职务违法或者职务犯罪，根据工作需要，可以依照规定查询、冻结涉案单位和个人的存款、汇款、债券、股票、基金份额等财产。 有关单位和个人应当配合。
:::
::: qst 问题
什么情况下监察机关可以采取搜查措施？
:::
::: notitle
- 监察机关可以对`涉嫌职务犯罪`的被调查人以及可能隐藏被调查人或者犯罪证据的人的身体、物品、住处和其他有关地方进行搜查。
- 在搜查时，应当出示搜查证，并有被搜查 人或者其家属等见证人在场。
:::
::: qst 问题
什么情况下监察机关可以采取查封、扣押措施？
:::
::: notitle
监察机关在调查过程中，可以调取、查封、扣押用以证明被调查人涉嫌违法犯罪的`财物、文件和电子数据`等信息。
:::
::: qst 问题
什么情况下监察机关可以采取技术调查措施？
:::
::: notitle
监察机关调查涉嫌`重大贪污贿赂`等职务犯罪，根据需要，经过严格的批准手续，可以采取技术调查措施，按照规定交有关机关执行。
:::

<bbt></bbt>
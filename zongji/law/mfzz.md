# 民法概论
## 民法的概念及调整对象
::: qst 问题
- 什么是民法？
- 民法调整的对象是？
:::
::: notitle
- 民法是调整`平等主体`之间财产关系和人身关系的法律规范的总称
  - 公民之间
  - 法人之间
  - 公民与法人之间
- 调整对象是自然人、法人和非法人组织之间的`人身关系和财产关系`。
:::


## 民法的基本原则
::: qst 问题
民法的基本原则有哪些？ 分别阐释其含义。
:::
::: notitle
- 平等原则
- 自愿原则
- 公平原则 
- `诚信`原则 
- `公序良俗`原则 
- `绿色`原则 
:::

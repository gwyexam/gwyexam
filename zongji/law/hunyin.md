# 婚姻法
## 无效和可撤销的婚姻
::: point 无效婚姻
1. `重婚`的
2. 有禁止结婚的`亲属关系`（直系血亲或者三代以内的旁系血亲）的；
3. 婚前患有医学上认为`不应当结婚的疾病`，婚后尚未治愈的；
4. 未到`法定婚龄`的`（男22，女20）`
:::
 
::: point 可撤销的婚姻
`因胁迫结婚的`，受胁迫的一方可以向婚姻登记机关或人民法院请求撤销该婚姻。
- 受胁迫的一方撤销婚姻的请求，应当自结记起`一年内`提出 
- 被非法限制人身自由的当事人请求撤销婚姻的，应当自`恢复人身自由之日起一年内`提出。
:::

## 诉讼离婚
::: point 调解无效的，应准予离婚
1. `重婚`或者有配偶者`与他人同居`的
2. 实施`家庭暴力`或虐待，`遗弃家庭成员`的
3. 有赌博、吸毒等`恶习屡教不改`的
4. 因感情不和`分居满二年`的
5. 其他导致夫妻感情破裂的情形。

- 一方`被宣告失踪`，另一方提出离婚诉讼的，应准予离婚
- 1,2中无过错方有权请求赔偿
:::

## 夫妻财产
::: point 共同财产
夫妻在婚姻关系在法期间所得的下列财产，归夫妻共同所有：
- 工资、奖金；
- 生产、经营的收益
- `知识产权的收益`；
- `继承或赠与所得的财产`，但遗嘱或赠与合同中确定只归夫或妻一方的财产除外
- 其他应当归共同所有的财产

夫妻对共同所有的财产，有平等的处理权
:::

::: point 一方的财产
- 一方的婚前财产
- 一方因身体受到伤害获得的`医疗费、残疾人生活补助费`等费用；
- 遗嘱或赠与合同中确定只归夫或妻一方的财产
- `一方专用的生活用品`
- 其他应当归一方的财产。

夫妻一方个人财产在婚后产生的收益，`除孳息和自然增值外`，应认定为夫妻共同财产。
:::

::: point 分割共同财产
婚姻关系存续期间，夫妻一方请求分割共同财产的，人民法院不予支持，但在下列重大理由且不损害债权人利益的除外：
- 一方有隐藏、转移、变卖、毁损、挥霍夫妻共同财产或者伪造夫妻共同债务等`严重损害夫妻共同财产利益行为的`；
- 一方负有法定扶养义务的人`患重大疾病需要医治`，另一方不同意支付相关医疗费用的。
:::

::: point 父母给子女的财产
- 婚后由`一方父母出资`为子女购买的不动产，产权登记在出资人子女名下的，视为只对自己子女一方的赠与，该不动产应认定为`夫妻一方的个人财产`
- 由`双方父母出资`购买的不动产，产权登记在一方子女名下的，该不动产可认定为双方按照各自父母的`出资份额共有`，但当事人另有约定的除外。
:::

<bbt></bbt>
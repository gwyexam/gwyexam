# 行政诉讼法
## 行政诉讼的受案范围
`受案范围与可申请行政复议的行政行为一致。`

::: point 排除范围
- 国防、外交等国家行为
- 行政法规、规章或者行政机关制定、发布的具有普遍约束力的决定、命令
- 行政机关对行政机关工作人员的奖惩、任免等决定
- 法律规定由行政机关最终裁决的行政行为
:::

::: point 不属于人民法院行政诉讼
下列行为不属于人民法院行政诉讼的受案范围：
- 公安、国家安全等机关依照刑事诉讼法的明确授权实施的行为。
- 调解行为以及法律规定的仲裁行为。
- 行政指导行为。
- 驳回当事人对行政行为提起申诉的重复处理行为，
- 行政机关作出的不产生外部法律效力的行为。
- 行政机关为作出行政行为而实施的准备、论证、研究、层报、咨询等过程性行为。
- 行政机关根据人民法院的生效裁判、协助执行通知书作出的执行行为，但行政机关扩大执行范围或者采取违法方式实施的除外。
- 上级行政机关基于内部层级监督关系对下级行政机关作出的听取报告、执法检查、督促履责等行为。
- 行政机关针对信访事项作出的登记、受理、交办、转送、复查、复核意见等行为
- 对公民、法人或者其他组织权利义务不产生实际影响的行为
:::

## 行政诉讼的管辖
### 1.级别管辖
::: notitle
- `中级人民法院`管辖下列第一审行政案件：
  - 对国务院部门或者县级以上地立人民政府所作的行政行为提起诉讼的案件；
  - 海关处理的案件
  - 本辖区内重大复杂的案件
  - 其他法律规定由中级人民法院管辖的案件。
- `高级人民法院`管辖本辖区内重大复杂的第一审行政案件；
- `最高人民法院`管辖全国范围重大复杂的第一审行政案件。
:::
### 2.地域管辖
行政诉讼的地域管辖可以外为一般地域管辖和特殊地域管辖。
::: point 一般地域管辖
行政案件由`最初做出行政行为的行政机关所在地`人民法院管辖。
:::

::: imt 特殊地域管辖
- 行政案件经过行政复议的，也可以由`复议机关所在地`的人民法院管辖。
- 因`不动产`提起诉讼的，由不动产所在地人民法院专属管错。
- 对`限制人身自由的行政强制措施`不服而提起诉讼的，由被告所在地或原告所在地人民法院管辖。原告所在地包括原户籍所在地、经常居住地和被限制人身自由地。
:::

### 3.裁定管辖
裁定管辖，指由人民法院作出裁定或决定来确定行政案件的管辖。具体而言，裁定管辖主要有三种，即移送管辖、指定管辖和管辖权的转移

## 行政诉讼的参加人
::: notitle
- 行政诉讼参加人包括`原告被告`和`第三人`以及他们的`诉讼代理人`
- 行政诉讼参与人包括当事人，代理人和证人、鉴定人、翻译人、勘验人等。
- 行政诉讼当事人指因`行政行为发生争议`，`以自己的名义`向人民法院起诉、应诉和参加诉讼，并受人民法院判决、裁定约束的公民、法人或者其他组织以及行政机关
::: 

::: point 1.行政诉讼的原告
- 起诉人须是自己的合法权益受到侵害的人
- 起诉人与行政行为之间具备法律上的利害关系。法律上的利害关系即法律上的权利、义务关系，起诉人合法权益所受到的影响、损害必须是由行政行为造成的，两者之间存在着相当因果关系。
:::

::: point 2.行政诉讼的被告
被告的确定要遵循两个规则：
- 被起诉人必须是被诉行政行为的实施者
- 行为实施者必须具在行政主体资格，即“谁主体，谁被告"。
:::

::: point 3.行政诉讼第三人
行政诉讼的第三人，指同提起行政诉讼的行政行为有利害关系，因而可能受到行政诉讼审理结果影响，依本人申请并经批准或由人民法院通知参加诉讼的公民、法人或其他组织。在行政诉讼中，应当追加被告，而原告不同意追加的，人民法院应当通知其以第三人的身份参加诉讼
:::

## 行政诉讼证据
::: notitle 
 - 书证、物证
 - 视听资料
 - 电子数据
 - 证人证言
 - 当事人的陈述
 - 鉴定意见
 - 勘验笔录、现场笔录
:::

## 行政诉讼程序
### 1.起诉
起诉，指公民、法人或者其他组织认为行政机关的行政行为侵犯其合法权益时，依法请求人民法院行使国家审判权给予司法救济的诉讼行为。
起诉期限的确定如下表所示：
::: notitle
- 一般起诉期限：`6个月`
- 只知道行政行为内容，不知道诉权或起诉期限：6个月，最长1年
- 不知道行政行为内容：6个月，涉及`不动产的不超过20年`，其他行政行为从作出之日起`不超过5年`
- 不履行法定：有履行期限的在期限届满后，无履行期限的在接到申请之日起两个月，属紧急情况的立即可诉
- 人身自由受限：人身自由受限时间不计
- 不服复议决定或复议机关逾期不作决定：收到复议决定书之日或复议期满之日起`15日内`
:::

### 2.受理
受理，指人民法院对公民、法人或者其他组织的起诉进行审查，对符合法律规定的起诉条件的案件决定立案审查的诉讼行为。

### 3.第一审程序
::: point 审理前的准备
- 组成合议庭
- 交换诉状
- 处理管辖异议
- 审查诉讼文书和调查收集证据
:::

::: point 庭审程序
- 必须采取言词重理的方式
- 公开审理为原则
- 审理行政案件一般不适用调解
:::

### 4.第二审程序
人民法院审理行政案件实行两审终审制度。
::: point 上诉与上诉受理
- 不服判决的，`15日内`提起上诉
- 不服裁定的，`10日内`提起上诉
:::
::: point 上诉案件的审理
- 二审法院审理行政案件，既要对原审法院的裁判是否合法进行审查，又要对被诉行政行为的合法性进行审查
- 二审法院审理行政案件，对被诉行政行为的合法性进行全面审查，不受上诉范围的限制
- 自收到上诉状之日起`3个月内`作出终审判决。
:::

### 5.二审裁判
::: notitle
1. 原判决、裁定认定事实清楚，适用法律、法规正确的，判决或者裁定驳回上诉，维持原判决
2. 原判决、裁定认定事实错误或者适用法律、法规错误的，依法改判、撤销或者变更。
3. 原判决认定基本事实不清、证据不足的，发回原审人民法院重审，或者查清事实后改判。
4. 原判决遗漏当事人或者违法缺席判决等严重违反法定程序的，裁定撤销原判决，发回原审人民法院重审
- 原审人民法院对发回重审的案件作出判决后，当事人提起上诉的，第二审人民法院不得再次发回
- 人民法院审理上诉案件，需要改变原审判决的，应当同时对被诉行政行为作出判决。
:::

<bbt></bbt>
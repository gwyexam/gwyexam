<div id="marxeco" class="svgcontainer">
  <img src="../../mindsvg/economic/e01.svg" />
</div>  

<div id="tizhi" class="svgcontainer">
  <img src="../../mindsvg/economic/e02.svg" />
</div>  

<div id="weiguan" class="svgcontainer">
  <img src="../../mindsvg/economic/e03.svg" />
</div>  

<div id="hongguan" class="svgcontainer">
  <img src="../../mindsvg/economic/e04.svg" />
</div>  

<div id="guoji" class="svgcontainer">
  <img src="../../mindsvg/economic/e05.svg" />
</div>

<div class="page2-nav">
  <a href="#marxeco">马克思主义政治经济学</a>
  <a href="#tizhi">经济体制</a>
  <a href="#weiguan">微观经济</a>
  <a href="#hongguan">宏观经济</a>
  <a href="#guoji">国际经济</a>
</div>
<bbt></bbt>
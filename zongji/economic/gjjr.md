# 国际金融
## 汇率
::: notitle
汇率，又称汇价，是`一个国家的货币折算成另一个国家货币的比率`，或者说是以外国货币来表示本国货币的价格。因此汇率表示的是两个国家货币之间的互换关系。
:::
### 标价方法
::: notitle
汇率主要有两种标价方法，分别是直接标价法和间接标价法。
- `直接标价法`是用`一个单位的外币`作为标准，折算为一定数额的本币来表示的汇率，`我国和国际上大多数国家都采用直接标价法`。
- 间接标价法是用一个单位的本币作为标准，折算为一定数额的外币来表示的汇率，英国一向使用间接标价法。
:::

::: point 汇率制度
世界上的汇率制度主要有固定汇率制度、浮动汇率制度以及钉住汇率制度
- `固定汇率制度`，是指政府用行政或法律手段选择一个基本参照物，并确定、公布和维持本国货币与该单位参照物之间的固定比价。充当参照物的东西可以是`黄金`，也可以是某种外国货币。
- `浮动汇率制度`，是指一国中央银行不规定本国货币与他国货币的官方汇率，听任汇率由外汇市场的供求关系自发地决定。
- `钉住汇率制度`，是指在本国的货币与其主要贸易伙伴国的货币之间确定一个固定的比价，随着种或几种货币进行浮动
:::

### 汇率的影响
::: notitle
汇率变动对一国经济的影响主要是通过`进出口`和`物价`来体现的
#### 本币汇率下降
- `促进出口`、`抑制进口`，引起进口商品国内价格上涨；
- 降低本币购买力，出国旅游、留学成本升高；
- 吸引外国游客，促进本国（地区）旅游业的发展。

#### 本币汇率上升
- `促进进口`，`抑制出口`的作用，引起进口商品国内价格下跌；
- 提高本币购买力，出国旅游、留学成本降低等
:::

## 国际金融市场
::: notitle
- 狭义的国际金融市场是指国际长短期资金借贷的场所。
- 广义的国际金融市场是指从事各种国际金融业务活动的场所。此种活动包括居民与非居民之间或非居民与非居民之间，一般指的概念是广义概念。例如，短期资金市场、长期资金市场、外汇市场、黄金交易市场等

国际金融市场的作用：`市场机制作用、保障作用、国际结算中心的作用、调节国际收支的作用、提供经济发展所需资金的作用`。
:::

## 国际金融危机
金融危机，指金融资产或金融机构或金融市场的危机，具体表现为`金融资产价格大幅下跌`，或金融机构倒闭或濒临倒闭，或某个金融市场如股市或债市暴跌等。

### 产生原因
::: notitle
国际金融危机的产生原因主要有：
- `经济过热导致生产过剩`
- `贸易收支巨额逆差`
- `外资的过度流入`
- `缺乏弹性的汇率制度和不当的汇率水平`
- `过早的金融开放`
:::

### 表现形式
国际金融危机一般有三种表现形式，即`货币危机、外债危机和银行危机`。
::: point 货币危机
货币危机，指一国货币在外汇市场面临大规模的抛压，从而导致该种`货币急剧贬值`，或者迫使货币当局花费大量的外汇储备和大幅度提高利率以维护现行汇率。
:::
::: point 外债危机
外债危机，指一国`不能履约偿还到期对外债务的本金和利息`，包括私人部门的债务和政府债务。其具有规模巨大、高度集中、处境艰难等特点。
:::
::: point 银行危机
银行危机，指由于对银行体系丧失信心导致个人和公司大量从银行提取存款的`挤兑现象`。

其主要表现在：银行不能如期偿付债务，或迫使政府出面，提供大规模援助，以避免违约现象的发生，一家银行的危机发展到一定程度，可能波及其他银行，从而引起整个银行系统的危机。
:::

<bbt></bbt>
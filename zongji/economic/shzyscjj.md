
# 社会主义市场经济
::: qst 问题
社会主义市场经济体制的含义是什么？
:::
::: point 含义
- 是`市场经济体制与社会主义制度的结合`，它不仅具有市场经济的一般规定和特征，同时又是与社会主义基本制度相结合的市场经济。
- 社会主义市场经济体制是在积极有效的`国家宏观调控`下，`市场对资源配置起决定性作用`，能够实现效率与公平的经济体制。
:::

## 社会主义市场经济基本特征
::: qst 问题
社会主义市场经济体制的基本特征有哪些？
:::
### 基本特征
::: notitle
#### ①所有制结构
- `以公有制为主体`，多种所有制经济共同发展
- 坚持公有制的主体地位，是社会主义市场经济的`基本标志`。 

#### ②分配制度
- 坚持`按劳分配为主体`，多种分配方式并存的制度。
- 把按劳分配和按生产要素分配结合起来，坚持效率优先，更加注重社会公平的原则。 

#### ③宏观调控
由于公有制为主体，因而国家对市场的调控具有较雄厚的`物质基础`，又有牢固的`政治基础`和广泛的`群众基础`。 

#### ④以实现共同富裕为根本目标
- 在社会主义初级阶段，我国允许和鼓励一部分地区和个人通过诚实劳动、合法经营先富起来，但这只是共同致富必然经历的过程。
- 最终`实现共同富裕`，才是发展社会主义市场经济的根本目标。
:::

### 公有制经济为主体的多种所有制经济
::: notitle
#### 公有制
- `国有`经济
- `集体`经济
- `混合所有制`经济中的国有成分和集体成分

#### 非公有制经济
- `个体`经济
- `私营`经济
- `外资`经济
- `混合所有制`经济中的非公有制成分
:::

## 社会主义市场经济基本框架
::: qst 问题
社会主义市场经济体制的基本框架是什么？
:::
::: point 三个“制度” 
- `建立现代企业制度`，是社会主义市场经济体制的`中心环节`。
- 建立以`按劳分配为主体`，多种分配方式并存的收入分配制度，是社会主义经济体制的`动力机制`。 
- 建立多层次的`社会保障制度`。这是社会主义市场经济体制的`安全阀和稳定器`。
:::
::: point 三个“体系” 
- 建立`全国统一开放的市场体系`。商品市场、资本市场、劳动力市场是市场体系的最基本内容，是市场体系的三大支柱。（前提） 
- 建立`以间接手段为主，完善的宏观调控体系`。（调节器）
- `健全和完善的法律体系`。（重要内容和组成部分）
:::

### 现代企业制度
::: qst 问题
- 现代企业制度的含义是什么?
- 有什么特征？
:::
::: notitle
所谓现代企业制度，是指`以法人财产权为基础`，`以有限责任制度为条件`，`以公司制为核心`的企业制度。
- `公司制`是现代企业制度的典型形式：`有限责任公司和股份有限公司`
- 现代企业制度包括：现代企业`产权制度`、现代企业`管理制度`和现代企业`组织制度`。
#### 现代企业制度的特征 
1. `产权清晰` 
2. `权责明确` 
3. `政企分开` 
4. `管理科学`
:::
::: qst 问题
- 公司的法人治理结构由什么组成？ 
- 公司的最高权力机构是？ 
- 公司的法人代表是？ 
- 公司执行监督职能的是？
:::
::: point 现代公司制企业的的法人治理结构
公司的法人治理结构是统治和管理公司的组织结构，由`股东会、董事会、监事会和经理机构`组成。 
#### 股东大会
- 也叫股东会，是公司的`最高权力机构`。
- 其主要职责是：选举和罢免董事会和监事会成员，制定和修改公司章程，审议和批准公司的财务预算及投资、收益分配方案， 决定公司的变更、分立、合并与解散等重大事宜。 
#### 董事会
- 董事会由股东会选举的董事组成，是公司的`法人代表`。
- 董事会对股东会负责，执行股东会的决议，决定公司的经营方针、经营范围以及关系公司全局的重大生产经营决策，任免公司的经理等。 
#### 监事会
- 监事会是股东大会领导下的公司的常设监察机构，执行`监督职能`。
- 监事会与董事会并立，独立地行使对董事会、总经理、高级职员及整个公司管理的监督权。
- 为保证监事会和监事的独立性，监事不得兼任董事和经理。监事会对股东会负责。 
#### 经理机构
- 经理机构是`经营管理者`（或经理人员）组成，对董事会负责。
- 在董事会授权范围内拥有对公司事务的管理权，负责处理公司的日常经营管理事务。
:::

### 分配制度
::: point 初次分配
初次分配指国民总收入（即国民生产总值）直接与生产要素相联系的分配;
初次分配注重效率，是按贡献进行的分配，就是要让有知识、善于创新并努力工作的人得到更多的劳务报酬，首先富裕起来；
- 以`税收`和`利润`等形式形成`财政收入`，如增值税、消费税；
- 以`工资`等形式分配给`劳动者`；
- `企业自留部分`
:::

::: point 再分配
再分配，指在初次分配结果的基础上各收入主体之间通过各种渠道实现现金或实物转移的一种收入再次分配过程，也是政府对要素收入进行再次调节的过程
- 个人所得税、财产税、股息红利和年底奖金等
- 社会保险、社会福利、社会优抚
:::

::: point 第三次分配
第三次分配是指动员社会力量，建立起社会救助、民间捐赠、慈善事业、志愿者行动等多种形式的制度和机制，是对于政府调控的补充。
:::

::: qst 问题
- 什么是按劳分配？ 
- 为什么实行按劳分配？
:::
::: point 按劳分配
按劳分配，即凡是有劳动能力的人都应尽自己的能力为社会劳动，社会以劳动作为分配个人消费品的尺度，按照劳动者提供的劳动数量和质量分配个人消费品，等量劳动获取等量报酬。
#### 按劳分配的必然性
1. `生产资料公有制`是实行按劳分配的`前提`。 
2. 社会主义公有制下`生产力的发展水平`是实行按劳分配的`物质基础`。 
3. 社会主义制度下人们`劳动的性质和特点`，是实行按劳分配的`直接原因`。
:::

::: qst 问题
- 什么是按生产要素分配？ 
- 为什么实行按要素分配？ 
- 有哪些分配种类
:::
::: point 按生产要素分配
在市场经济条件下，生产要素的使用者根据各种生产要素在生产经营过程中发挥的贡献大小，按照一定比例，对生产要素的所有者支付相应报酬的一种分配方式，是生产要素私有制在经济上的实现。

#### 按要素分配存在的必然性 
`在按劳分配为主体的前提下`，`把按劳分配和按生产要素分配结合起来`，是社会主义市场经济发展的必然要求，也是马克思主义分配理论的重大突破。

#### 分配要素的种类 
- 按`劳动要素`分配的主要形式：工资、津贴和奖金。
- 按`技术、信息要素`分配：是科技工作者、信息工作者提供新技术和信息资料取得的收入。 
- 按`资本要素`分配的形式有：利息、股息、债券、股票交易收入按土地要素分配，是凭借土地取得的收入。 
- 按`管理要素`分配，是企业的管理人才凭借其管理才能在生产经营中的的贡献而参与分配的方式。
:::

### 社会保障制度
::: qst 问题
什么是社会保障制度？有什么意义？
:::
::: notitle
社会保障是保障人民生活、调节社会分配的一项基本制度。建立和完善社会保障体系，是人民安居乐业、社会公平和谐、国家长治久安的重要基础。
:::

::: qst 问题
- 什么是社会保险？ 
- 什么是社会福利？ 
- 什么是社会救济？ 
- 什么是社会优抚？
:::
::: point 内容
#### 社会救济
社会救济是国家和社会对于无法维持生活的公民提供的`物质救助`的保障制度，属于`最低层次`的社会保障措施。 
#### 社会优抚
社会优抚与安置是国家和社会对法定的对象`给予抚恤和优待`的保障制度。这些对象包括现役军人、军属、退伍、复员、转业、残废军人、军烈属等`特殊社会成员`。
#### 社会福利
社会福利是国家或社会向全体公民提供`资金帮助和优惠服务`的保障制度。包括公共福利、社会福利和集体福利。
#### 社会保险
- 社会保险是国家通过立法强制建立社会保险基金，对`参加劳动关系的劳动者`在丧失劳动能力或失业时给予必要的物质帮助的保障制度。
- `社会保险`包括养老保险、失业保险、医疗保险、工伤保险、生育保险等方面。它是`社会保障制度的核心内容`。 
:::

::: qst 问题
社会保障制度的方针有哪些？
:::
::: point 方针
1. `全覆盖`，就是要根据社会保障制度的类型实现最广泛的覆盖。 
2. `保基本`，就是要坚持尽力而为、量力而行的原则，根据我国经济社会发展状况合理确定社会保障待遇水平，保障基本的生活需求。 
3. `多层次`，就是要以社会救助为保底层、社会保险为主体层，积极构建以企业（职业）年金等补充社会保险和商业保险为补充层的多层次社会保障体系。 
4. `可持续`，就是要立足制度的长远发展，统筹协调，探索
:::

<bbt></bbt>
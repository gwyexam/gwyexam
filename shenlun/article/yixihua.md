# 每日一习话
## 基层党组织建设
::: notitle
#### 习近平：要强化政治引领，发挥党的群众工作优势和党员先锋模范作用，引领基层各类组织自觉贯彻党的主张，确保基层治理正确方向。

这段话出自2018年7月3日习近平总书记在全国组织工作会议上的讲话。

党的基层组织是党在社会基层组织中的战斗堡垒，是党的全部工作和战斗力的基础，承担着宣传和执行党的路线、方针、政策等多项重要使命，在推动基层社会治理现代化方面发挥着重要作用。

习近平总书记的讲话，为加强基层党组织建设，构建党组织统一领导、各类组织积极协同、广大群众广泛参与的基层治理体系指明了方向。

群众事，无小事。基层是社会和谐稳定的基础。在基层社会治理中，基层党组织承担着领导基层治理力量、构筑基层治理体系、优化基层治理结构、提升基层治理能力的职责。在长期治国理政实践中，注重发挥基层党组织在基层社会治理中的创造力、凝聚力、战斗力，是我国社会治理的显著特点和独特优势。

基层党组织是党执政大厦的地基。着眼于社会治理重心向基层下移和构建基层社会治理新格局，就要以提升组织力为重点，推进党的基层组织设置和活动方式创新，突出政治功能，把企业、农村、机关、学校、科研院所、街道社区、社会组织等基层党组织建设成为领导基层治理的坚强战斗堡垒，让人民群众有更多的获得感、幸福感、安全感，筑牢社会长治久安的压舱石
:::

## 扶贫攻坚
::: notitle
#### 习近平：我一直强调扶贫既要扶智，又要扶志，一个是智慧，一个是志气，不光是输血，还要建立造血机制，脱贫后生活还要不断芝麻开花节节高。

这段话出自2019年9月17日习近平在河南考察时的讲话。

2020年是决胜全面建成小康社会、决战脱贫攻坚之年，中华民族实现消减绝对贫困的千年梦想已进入最后倒计时。不过，脱贫不是终点，人们对美好生活的向往永不停歇。

决战脱贫攻坚，靠单纯给钱、给物的输血式扶贫只能解决当前突出问题，还要着眼长远，防止贫困人口因多种原因返贫，建立造血机制才是关键。

贫困人口在脱贫后如何保持长效稳定的发展后劲？内生动力是重中之重。实现长期稳定脱贫，需要提升贫困地区的内生发展机制，根据当地自然条件发展特色产业，通过龙头企业、专业合作社、集体经济组织、当地能人等多元主体带动，打造农业、加工业、旅游业等为一体的产业融合发展机制，让农民获得主动发展的可行路径，提升对自身能力的自信心。

同时，我们还要继续稳固提升贫困地区的教育支撑力量，避免出现贫困代际相传现象。在总结教育脱贫攻坚先进经验基础上，通过完善教育基础设施、夯实教育人才队伍等防止因学返贫现象出现；通过引导宣传、信息交流、平台搭建、培训带动等方式，改变贫困人口封闭落后的思想和观念，从根本上将被动式扶持扭转为主动式发展。

行百里者半九十，贫困人口脱贫后的可持续发展任重而道远，只有拥有永不放弃的自信心和自我发展的生命力，才能照亮通往美好生活的道路，才能实现脱贫后生活芝麻开花节节高。 
:::

::: notitle
#### 习近平：要发挥数字减贫作用，提升数字包容性，为中小企业、妇女、青年等弱势群体提供更多脱贫致富机会。

 随着我国现代信息水平的提升，数字减贫在我国脱贫攻坚中取得了较大突破和成效，数字发挥的创新和驱动作用至关重要，数字减贫模式在越来越多的贫困地区落地生根，一些成功经验也为全球减贫事业提供了借鉴。

“工欲善其事，必先利其器。”发挥数字减贫的带动作用，需要完善网络基础设施，通光纤、建基站，并通过硬化通村公路，设立快递服务站等，打通物流配送的“最后一公里”，为乡村数字发展提供基础性条件。

同时，我们也要借鉴数字发展的成熟模式，依托互联网购物、电商销售、直播带货等，为乡村创业青年、妇女等群体发展当地特色产业带来契机，带动产业链上下游发展，创造更多就业机会。

站在新的历史起点上，开启全面建设社会主义现代化国家新征程，我们要进一步发挥数字的影响力，以特殊群体为对象，实现精准性对接。

通过电子商务、远程教育等，增强对农村妇女、儿童、老人等特殊群体的信息供给；通过大数据、区块链等新兴技术有效整合农村数据资源，缓解中小企业融资难、融资贵等问题；创新信用评价机制，构建中小企业的风险共担机制……数字经济时代，数字化将是我们巩固脱贫成果、实施乡村振兴的强大稳定器、助推器。 
:::


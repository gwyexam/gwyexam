# 议论文写作概述
::: tip 解题方法
- 总论点和分论点从材料中来。
- 参考给定资料：议论文的总论点和分论点必须来源于给定资料。
- 不拘泥于给定资料：必须要有材料以外的论据，比如事例、名人名言等。
:::

::: point 内在要求
- 论点:主旨明确，符合要求
- 论据充分，论证有力
- 结构合理，语言流畅
::: details
![avatar](../../imgs/subjects/079.jpg)
![avatar](../../imgs/subjects/080.jpg)  
![avatar](../../imgs/subjects/081.jpg)
![avatar](../../imgs/subjects/082.jpg)
:::

::: point 形式要求
- 字迹端正，横平竖直
- 字数正负10%，符合文章要求
- 卷面整洁
- 内容
  - 标题醒目，段落匀称
  - 开头结尾各150字左右
  - 中间部分段落要匀称
  - 内部相互呼应，浑然一体
:::
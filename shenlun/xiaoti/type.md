# 材料的分类
## 权威理论性材料
::: notitle 
- 副省级以上领导的言论
- 副省级以上党委和政府的文件
- 一般观点性材料
::: details 示例
![avatar](../../imgs/subjects/001.jpg)
![avatar](../../imgs/subjects/002.jpg)
:::

::: point 摘抄方法
- 客观题：直接摘抄
- 主观题： 作为论据
- 可以用来引申话题
:::

## 一般观点性材料
::: notitle 
- 专家学者的观点
- 基层政府的观点
- 舆论媒体的观点
- 群众网友的观点
::: details 示例
![avatar](../../imgs/subjects/003.jpg)
![avatar](../../imgs/subjects/004.jpg)
![avatar](../../imgs/subjects/005.jpg)
![avatar](../../imgs/subjects/006.jpg)
:::

::: point 摘抄方法
- 辨真伪
- 若为真可摘抄使用
:::


## 事实性材料
::: notitle
- 事实陈述
- 个别案例

::: details 示例
![avatar](../../imgs/subjects/007.jpg)
![avatar](../../imgs/subjects/008.jpg)
:::

::: point 摘抄方法
- 摘抄
- 总结之后摘抄
- 摘抄首尾句，总结性语言
:::

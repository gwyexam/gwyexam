# 应用文写作
应用文是党政机关、事业单位、企业和个人用以处理各种公私事务、传递交流信息、解决实际问题所使用的具有直接实用价值、格式规范、语言简约的多种文体的统称。

::: notitle
- 给定身份，给定文种，明确任务
- 量词(一份，一篇) + 的 + 文种
:::

## 形式要求
::: point 格式正确
- 标题：(发文机关)+事由+文种
- 主送机关：级别必须一致，行政区域在前，职能部门在后
- 正文：是什么，为什么，怎么办
- 发文机关： 多个发文机关上下排列

::: details
![avatar](../../imgs/subjects/055.jpg)
![avatar](../../imgs/subjects/056.jpg)
![avatar](../../imgs/subjects/057.jpg)
![avatar](../../imgs/subjects/058.jpg)  
:::

::: point 语言规范
- 准确：不能出现大概，大约等词语
- 简明：简明扼要
:::

## 审题
::: notitle
- 明确文种
- 明确身份
- 明确对象
- 明确目的
- 明确范围
::: details
![avatar](../../imgs/subjects/060.jpg)
:::

## 写作内容构成 🔶
::: tip 根据写作目的预判写作内容构成
- 背景：如果没有背景可以以问题来呈现背景
- 是什么：含义
- 为什么：原因，问题，成绩，意义，危害
- 怎么做：对策
- 行文要求
:::

::: details 示例
![avatar](../../imgs/subjects/061.jpg)
![avatar](../../imgs/subjects/062.jpg)
![avatar](../../imgs/subjects/063.jpg)
:::

::: details 案例1
![avatar](../../imgs/subjects/064.jpg)
![avatar](../../imgs/subjects/069.jpg)
![avatar](../../imgs/subjects/070.jpg)
![avatar](../../imgs/subjects/071.jpg)
:::

::: details 案例2
![avatar](../../imgs/subjects/072.jpg)
![avatar](../../imgs/subjects/073.jpg)
![avatar](../../imgs/subjects/073.jpg)
![avatar](../../imgs/subjects/075.jpg)
![avatar](../../imgs/subjects/076.jpg)
:::
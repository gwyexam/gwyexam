# 概括题
提炼材料中的主题要素如问题，影响，原因，对策等要点来揭示材料本质的一类题目。

## 题型概述
::: notitle
#### 审题
范围、题型、主题、要求

#### 题型界定
`概括、总结、概述、归纳、指出`等 + 具体要素(表现，原因，影响，对策)
:::

## 答题方法 🔶
::: point 提炼要点 
- 去论据
- 去数据
- 去修饰
:::

::: point 概括要点 
数据 大小 多少

::: imt 如何概括
- 概括是由一个外延较小的概念到一个外延较大的概念
- 概念之后的概念之间应该是属种关系
- 扩大外延不能是无限的，扩大一级即可

::: details 示例
![avatar](../../imgs/subjects/027.jpg)
![avatar](../../imgs/subjects/028.jpg)
![avatar](../../imgs/subjects/029.jpg)
![avatar](../../imgs/subjects/030.jpg)
:::


::: point 归纳整理
答案 分门别类
::: details 示例
![avatar](../../imgs/subjects/031.jpg)
:::

## 题型拓展
`综合概括`是多个单一概括的集合，即对多个要素进行概括
::: details 示例
![avatar](../../imgs/subjects/038.jpg)
![avatar](../../imgs/subjects/039.jpg)

#### 案例
![avatar](../../imgs/subjects/036.jpg)
![avatar](../../imgs/subjects/037.jpg)
:::
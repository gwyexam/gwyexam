# 对策题
## 题型概述
提出问题，给定身份，明确任务.
::: point 要素词辨析
- 对策 解决问题，削弱负面影响，我能做
- 建议 我不能做，他人做
- 措施 对策的具体体现，侧重实施和展开
- 方案 整体性，全面性，系统性
::: details
![avatar](../../imgs/subjects/043.jpg)
![avatar](../../imgs/subjects/044.jpg)
![avatar](../../imgs/subjects/045.jpg)
:::

## 对策要求
::: notitle
针对性，可行性，合理，具体，有条理
::: details
![avatar](../../imgs/subjects/046.jpg)
:::

## 对策来源
::: point 概括 🔶
- 权威材料：直接抄
- 事例经验：正面案例提取它的做法
- 观点建议：从观点中摘抄要点

::: details 示例
![avatar](../../imgs/subjects/047.jpg)
![avatar](../../imgs/subjects/048.jpg)
![avatar](../../imgs/subjects/049.jpg)
![avatar](../../imgs/subjects/050.jpg)
![avatar](../../imgs/subjects/051.jpg)
:::

::: notitle 
- 问题反推
- 原因反推
- 凭自己经验
::: details 示例
![avatar](../../imgs/subjects/052.jpg)
:::

## 对策书写 🔶
::: point 如何全面的表述对策？
- 主体——对策的实施者
- 方式——对策的途径
- 对象——对策的受动者
- 内容——对策的具体内容
- 目的——对策的预期效果

::: details 示例
![avatar](../../imgs/subjects/053.jpg)
![avatar](../../imgs/subjects/054.jpg)
:::
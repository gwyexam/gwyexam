# 材料要素
::: imt 解题攻略
主题在第一则材料中就出现或者在权威理论性材料。
:::

## 六大要素
::: notitle 
- 成绩：正面色彩词汇
- 问题：负面词汇
- 原因：看关联词
- 意义：正面色彩词汇
- 危害：负面词汇
- 对策：标志性动词

::: details 示例
![avatar](../../imgs/subjects/009.jpg)
![avatar](../../imgs/subjects/010.jpg)
![avatar](../../imgs/subjects/013.jpg)
![avatar](../../imgs/subjects/011.jpg)
![avatar](../../imgs/subjects/012.jpg)
![avatar](../../imgs/subjects/014.jpg)
:::

## 提取要素的方法 🔶
::: notitle 
- 关注首尾句
- 关注要素词
- 关注观点句
- 关注关联词
  - 因果
  - 转折 
  - 递进
  - 并列
::: details 示例
![avatar](../../imgs/subjects/015.jpg)
![avatar](../../imgs/subjects/016.jpg)
![avatar](../../imgs/subjects/017.jpg) 
![avatar](../../imgs/subjects/018.jpg) 
![avatar](../../imgs/subjects/019.jpg) 
![avatar](../../imgs/subjects/020.jpg)
![avatar](../../imgs/subjects/021.jpg) 
![avatar](../../imgs/subjects/022.jpg)
![avatar](../../imgs/subjects/023.jpg)
![avatar](../../imgs/subjects/024.jpg)
:::
# gwyexam

我也只是公考萌新，给刚入门的同学一点点指引，大神可以飘过了

### 介绍
- 公务员及事业单位考试备考笔记
- 事业单位的内容也可以当做公务员考试的常识部分去浏览
- 面试部分是一些基础理论知识，还是建议报班吧 


### 使用说明
- 基于vuepress生成文档
- 建议使用vscode来编辑

#### 初始化
vscode的终端面板运行，安装依赖
```js
// 没有nodejs的需先安装node
npm install
```

#### 启动
vscode下使用npm脚本，点击dev处的运行按钮即可启动

#### 部署
- npm脚本，点击build即可打包，打包好生成在.vuepress目录下dist
- 拷贝dist到根目录
- gitee上新建一个仓库命名为gwyexam
- 上传代码后使用仓库的服务中的gitee pages，部署目录选择dist，即可生成静态页面了。
